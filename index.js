class Card {
    constructor(name, email, title, body, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
    }

    render() {
        const card = document.createElement('div');
        card.className = 'card';

        const button = document.createElement('a');
        button.href = '#';

        card.append(button);

        const cardHeader = document.createElement('div');
        cardHeader.className = 'card-header';
        cardHeader.innerHTML = `
            <h2>${this.name}</h2>
            <span>${this.email}</span>
        `;
        card.append(cardHeader);

        const cardBody = document.createElement('div');
        cardBody.className = 'card-body';
        cardBody.innerHTML = `
            <h2>${this.title}</h2>
            <p>${this.body}</p>
        `;
        card.append(cardBody);

        document.body.append(card);

        button.addEventListener('click', (e) => {
            e.preventDefault();
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method: 'DELETE',
            })
            .then(({ ok }) => {
                if (ok) {
                    card.remove();
                }
            });
        });
    }
};

function createCard() {
    fetch('https://ajax.test-danit.com/api/json/users')
    .then(res => res.json())
    .then(users => {
        fetch('https://ajax.test-danit.com/api/json/posts')
        .then(res => res.json())
        .then(posts => {
            users.forEach(({ name, email, id }) => {
                posts.forEach(({ title, body, userId, id: postId }) => {
                    if (id === userId) {
                        new Card(name, email, title, body, postId).render();
                    }
                });
            });
        });
    });
}

createCard();